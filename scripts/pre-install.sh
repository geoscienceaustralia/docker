#!/bin/bash

#==============================================================
echo "pre-install.sh"
#==============================================================

# This file is used to install required  packages.
# We will restart at the end of the packer build to ensure all
# updates are applied.

#--------------------------------------------------------------
echo "Install updates"
#--------------------------------------------------------------

sudo apt-get update
sudo apt-get -y dist-upgrade

#--------------------------------------------------------------
echo "Install docker"
#--------------------------------------------------------------
sudo apt-get update
sudo apt-get install -y awscli

#--------------------------------------------------------------
echo "Install docker"
#--------------------------------------------------------------

# Configure apt-get to allow https repositories.
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Get the official docker repo
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Configure the stable branch
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install docker-ce
sudo apt-get update
sudo apt-get install -y docker-ce
sudo apt-get install -y docker-compose

#--------------------------------------------------------------
echo "Install Groovy"
#--------------------------------------------------------------

sudo apt-get install -y groovy