#!/bin/bash

#==============================================================
echo "post-install.sh"
#==============================================================

# This file is used to make any configuration changes.

#--------------------------------------------------------------
echo "Fix Broken Locale"
#--------------------------------------------------------------

# The default AMI is missing locale data, so we have to add it
sudo locale-gen en_AU.UTF-8

#--------------------------------------------------------------
echo "Secure shared memory"
#--------------------------------------------------------------

# Shared memory can be used to attack a running service secure it
sudo echo "tmpfs     /run/shm     tmpfs     defaults,noexec,nosuid     0     0"\
| sudo tee --append /etc/fstab

#--------------------------------------------------------------
echo "Harden network with sysctl settings"
#--------------------------------------------------------------

# Provent source routing of incoming packets
sudo cat /tmp/files/sysctl.conf | sudo tee --append /etc/sysctl.conf

# set memory so docker can run 
sudo sysctl -w vm.max_map_count=262144
## Reload sysctl
sudo sysctl -p

#--------------------------------------------------------------
echo "Prevent IP Spoofing"
#--------------------------------------------------------------

sudo echo "nospoof on" | sudo tee --append /etc/host.conf
