# Packer Simple Webserver
> This will create an ami with apache2 installed

## Packages
1. Docker
1. Goss (for running tests)

## Usage
1. Download and install Packer from [packer.io](http://packer.io)
4. `packer build build.json`
3. Deploy a new EC2 instance using the AMI that was created in step 2.
4. Head to the public DNS record for the new instance to see your 'Hello, world!'

## Advanced usage
1. add home ips to fail2ban whitelist:
2. `export HOME_IP_HTML=yourip`
3. `packer build build.json`

1. add html and ssh ips to fail2ban whitelist
2. `export HOME_IP_HTML=yourip`
2. `export HOME_IP_SSH=yoursship`
3. `packer build build.json`
